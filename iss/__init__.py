import urllib
import json


class Imp:
    def __init__(self):
        urllib.request.urlcleanup()
        self.response = urllib.request.urlopen(urllib.request.Request("http://api.open-notify.org/iss-now.json"))
        self.obj = json.loads(self.response.read())

    def lat(self):
        return self.obj['iss_position']['latitude']

    def lon(self):
        return self.obj['iss_position']['longitude']
