import setuptools

setuptools.setup(
    name='iss_api',
    version='1.1',
    description='ISS Data Wrapper',
    packages=setuptools.find_packages(),
    author='Cakebot Team',
    author_email='cakebot@rdil.rocks',
    url='https://github.com/cakebotpro/iss_api'
)
